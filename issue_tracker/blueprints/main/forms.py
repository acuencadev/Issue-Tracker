from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField
from wtforms.validators import DataRequired, Length


class LoginForm(FlaskForm):
    username = StringField('Username', [
        DataRequired(message="Please enter an username.")
    ])
    password = PasswordField('Password', [
        DataRequired(message="Please enter a password."),
        Length(min=6, message="The password should be at least 6 characters long.")
    ])
    submit = SubmitField('Sign In')
