from flask import Blueprint, redirect, render_template, request, url_for
from flask_login import current_user, login_required, login_user, logout_user

from issue_tracker.blueprints.main.forms import LoginForm
from issue_tracker.models import User


main_bp = Blueprint('main_bp', __name__)


@main_bp.route("/")
@login_required
def index_get():
    return render_template("main/index.html")


@main_bp.route("/login", methods=["GET"])
def login_get():
    if current_user.is_authenticated:
        return redirect(url_for('main_bp.index_get'))

    form = LoginForm()

    return render_template("main/login.html", form=form)


@main_bp.route("/login", methods=["POST"])
def login_post():
    form = LoginForm()

    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()

        if user and user.validate_password(form.password.data):
            login_user(user)
            next_ = request.args.get('next')

            return redirect(next_ or url_for('main_bp.index_get'))

    return redirect(url_for("main_bp.login_get", form=form))


@main_bp.route("/logout", methods=['GET'])
@login_required
def logout_get():
    logout_user()

    return redirect(url_for('main_bp.login_get'))
