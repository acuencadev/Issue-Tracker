from flask import redirect, url_for

from issue_tracker.extensions import login_manager
from issue_tracker.models import User


@login_manager.user_loader
def load_user(user_id):
    if user_id is not None:
        return User.query.get(user_id)

    return None


@login_manager.unauthorized_handler
def unauthorized():
    return redirect(url_for('main_bp.login_get'))
