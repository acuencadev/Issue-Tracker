from typing import Optional, List

from issue_tracker.models import db, Project


def create_project(name: str, description: str, short_tag: str, rank: int,
                   owner_id: int) -> Optional[Project]:
    project = Project(name=name, description=description, short_tag=short_tag, rank=rank,
                      owner_id=owner_id)

    db.session.add(project)
    db.session.commit()

    return project


def update_project(short_tag: str, name: str, description: str,
                   rank: int) -> Optional[Project]:
    project = get_project_by_short_tag(short_tag)

    if not project:
        return None

    project.name = name
    project.description = description
    project.short_tag = short_tag
    project.rank = rank

    db.session.commit()

    return project


def get_project_by_id(project_id: int) -> Optional[Project]:
    return Project.query.get(project_id)


def get_project_by_short_tag(short_tag: str) -> Optional[Project]:
    return Project.query.filter_by(short_tag=short_tag).first()


def get_all_projects() -> List[Project]:
    return Project.query.all()


def delete_project_by_short_tag(short_tag: str) -> bool:
    project = get_project_by_short_tag(short_tag)

    if not project:
        return False

    db.session.delete(project)
    db.session.commit()

    return True
