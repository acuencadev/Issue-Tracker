from typing import Optional, List

from issue_tracker.models import db, IssueType


def create_issue_type(name: str, rank: int) -> Optional[IssueType]:
    issue_type = IssueType(name=name, rank=rank)

    db.session.add(issue_type)
    db.session.commit()

    return issue_type


def update_issue_type(name: str, rank: int) -> Optional[IssueType]:
    issue_type = get_issue_type_by_name(name)

    if not issue_type:
        return None

    issue_type.name = name
    issue_type.rank = rank

    db.session.commit()

    return issue_type


def get_issue_type_by_id(issue_id: int) -> Optional[IssueType]:
    return IssueType.query.get(issue_id)


def get_issue_type_by_name(name: str) -> Optional[IssueType]:
    return IssueType.query.filter_by(name).first()


def get_all_issue_types() -> List[IssueType]:
    return IssueType.query.all()


def delete_issue_type_by_name(name: str) -> bool:
    issue_type = get_issue_type_by_name(name)

    if not issue_type:
        return False

    db.session.delete(issue_type)

    return True
