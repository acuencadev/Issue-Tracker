from flask_wtf import FlaskForm
from wtforms import StringField, TextAreaField, SelectField, SubmitField
from wtforms.validators import DataRequired


class ProjectForm(FlaskForm):
    name = StringField('Name', [
        DataRequired()
    ])
    short_tag = StringField('Short Tag', [
        DataRequired()
    ])
    description = TextAreaField('Description')
    rank = SelectField('Rank', choices=[(x, x) for x in range(1, 11)], coerce=int)
    submit = SubmitField('Save')


class IssueTypeForm(FlaskForm):
    name = StringField('Name', [
        DataRequired()
    ])
    rank = SelectField('Rank', choices=[(x, x) for x in range(1, 11)], coerce=int)
