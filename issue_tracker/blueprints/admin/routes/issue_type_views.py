from flask import abort, Blueprint, redirect, render_template, url_for
from flask_login import login_required

from issue_tracker.blueprints.admin.forms import IssueTypeForm
import issue_tracker.blueprints.admin.services.issue_type_service as issue_type_service


issue_type_bp = Blueprint("issue_type_bp", __name__)


@issue_type_bp.route("/admin/issue-types", methods=['GET'])
@login_required
def index_get():
    issue_types = issue_type_service.get_all_issue_types()

    return render_template("admin/issue-type/index.html", issue_types=issue_types)


@issue_type_bp.route("/admin/issue-types/create", methods=['GET'])
@login_required
def create_get():
    form = IssueTypeForm()

    return render_template("admin/issue-type/create.html", form=form)


@issue_type_bp.route("/admin/issue-types/create", methods=['POST'])
@login_required
def create_post():
    form = IssueTypeForm()

    if form.validate_on_submit():
        new_issue_type = issue_type_service.create_issue_type(
            name=form.name.data,
            rank=form.rank.data)

        if new_issue_type:
            return redirect(url_for("issue_type_bp.view_get", name=new_issue_type.name))

    return render_template("admin/issue-type/create.html", form=form)


@issue_type_bp.route("/admin/issue-types/<string:name>", methods=['GET'])
@login_required
def view_get(name):
    issue_type = issue_type_service.get_issue_type_by_name(name)

    if not issue_type:
        abort(404)

    return render_template("admin/issue-type/details.html", issue_type=issue_type)


@issue_type_bp.route("/admin/issue-types/<string:name>/edit", methods=['GET'])
@login_required
def edit_get(name):
    issue_type = issue_type_service.get_issue_type_by_name(name)

    if not issue_type:
        abort(404)

    form = IssueTypeForm()
    form.name.data = issue_type.name
    form.rank.data = issue_type.rank

    return render_template("admin/issue-type/edit.html", form=form, issue_type=issue_type)


@issue_type_bp.route("/admin/issue-types/<string:name>/edit", methods=['POST'])
@login_required
def edit_post(name):
    issue_type = issue_type_service.get_issue_type_by_name(type)

    if not issue_type:
        abort(404)

    form = IssueTypeForm()

    if form.validate_on_submit():
        issue_type_service.update_issue_type(name=form.name.data,
                                             rank=form.rank.data)
        return redirect(url_for("issue_type_bp.view_get", name=name))

    return redirect(url_for("issue_type_bp.edit_get", name=name, form=form))


@issue_type_bp.route("/admin/issue-types/<string:name>/delete", methods=['POST'])
@login_required
def delete_post(name):
    issue_type = issue_type_service.get_issue_type_by_name(name)

    if not issue_type:
        abort(404)

    issue_type_service.delete_issue_type_by_name(name)

    return redirect(url_for("issue_type_bp.index_get"))
