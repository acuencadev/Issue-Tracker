from flask import abort, Blueprint, redirect, render_template, url_for
from flask_login import login_required, current_user

from issue_tracker.blueprints.admin.forms import ProjectForm
import issue_tracker.blueprints.admin.services.project_service as project_service


project_bp = Blueprint('project_bp', __name__)


@project_bp.route("/admin/projects", methods=['GET'])
@login_required
def index_get():
    projects = project_service.get_all_projects()

    return render_template("admin/project/index.html", projects=projects)


@project_bp.route("/admin/projects/create", methods=['GET'])
@login_required
def create_get():
    form = ProjectForm()

    return render_template("admin/project/create.html", form=form)


@project_bp.route("/admin/projects/create", methods=['POST'])
@login_required
def create_post():
    form = ProjectForm()

    if form.validate_on_submit():
        new_project = project_service.create_project(name=form.name.data,
                                                     short_tag=form.short_tag.data,
                                                     description=form.description.data,
                                                     rank=form.rank.data,
                                                     owner_id=current_user.id)

        if new_project:
            return redirect(url_for("project.view_get", short_tag=new_project.short_tag))

    return redirect(url_for("project_bp.create_get", form=form))


@project_bp.route("/admin/projects/<string:short_tag>", methods=['GET'])
@login_required
def view_get(short_tag):
    project = project_service.get_project_by_short_tag(short_tag)

    if not project:
        abort(404)

    return render_template("admin/project/details.html", project=project)


@project_bp.route("/admin/projects/<string:short_tag>/edit", methods=['GET'])
@login_required
def edit_get(short_tag):
    project = project_service.get_project_by_short_tag(short_tag)

    if not project:
        abort(404)

    form = ProjectForm()
    form.name.data = project.name
    form.short_tag.data = project.short_tag
    form.description.data = project.description
    form.rank.data = project.rank

    return render_template("admin/project/edit.html", form=form, project=project)


@project_bp.route("/admin/projects/<string:short_tag>/edit", methods=['POST'])
@login_required
def edit_post(short_tag):
    project = project_service.get_project_by_short_tag(short_tag)

    if not project:
        abort(404)

    form = ProjectForm()

    if form.validate_on_submit():
        project_service.update_project(name=form.name.data,
                                       short_tag=form.short_tag.data,
                                       description=form.description.data,
                                       rank=form.rank.data)

        return redirect(url_for("project_bp.view_get", short_tag=short_tag))

    return redirect(url_for("project_bp.edit_get", short_tag=short_tag, form=form))


@project_bp.route("/admin/projects/<string:short_tag>/delete", methods=['POST'])
@login_required
def delete_post(short_tag):
    project = project_service.get_project_by_short_tag(short_tag)

    if not project:
        abort(404)

    project_service.delete_project_by_short_tag(short_tag)

    return redirect(url_for("project_bp.index_get"))
