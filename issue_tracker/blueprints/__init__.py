from issue_tracker.blueprints.main.routes import main_bp
from issue_tracker.blueprints.admin.routes.project_views import project_bp
from issue_tracker.blueprints.admin.routes.issue_type_views import issue_type_bp

import issue_tracker.blueprints.login_manager_loader
