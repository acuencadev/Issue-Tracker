from flask import Flask


def create_app(override_settings=None) -> Flask:
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_object('issue_tracker.config.Config')
    app.config.from_pyfile('settings.py', silent=True)

    if override_settings:
        app.config.update(override_settings)

    register_extensions(app)
    register_blueprints(app)

    return app


def register_extensions(app: Flask) -> None:
    from issue_tracker.extensions import alembic, csrf, db, login_manager
    from issue_tracker.models import User, Project, IssueType

    alembic.init_app(app)
    csrf.init_app(app)
    db.init_app(app)
    login_manager.init_app(app)

    return None


def register_blueprints(app: Flask) -> None:
    with app.app_context():
        from issue_tracker.blueprints import (main_bp, project_bp, issue_type_bp)

        app.register_blueprint(main_bp)
        app.register_blueprint(project_bp)
        app.register_blueprint(issue_type_bp)

    return None
