import datetime

from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash

from issue_tracker.extensions import db


class User(UserMixin, db.Model):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(50), unique=True, index=True)
    fullname = db.Column(db.String(100))
    password = db.Column(db.String(80))
    email = db.Column(db.String(100), index=True)

    created_at = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    last_updated_at = db.Column(db.DateTime, onupdate=datetime.datetime.utcnow)
    last_updated_by = db.Column(db.Integer, nullable=True)

    projects = db.relationship('Project',
                               foreign_keys='Project.owner_id',
                               backref='owner',
                               lazy=True)

    @property
    def unhashed_password(self):
        raise ValueError("You cannot get the unhashed password!")

    @unhashed_password.setter
    def unhashed_password(self, unhashed_password: str):
        self.password = generate_password_hash(unhashed_password, method='sha256')

    def validate_password(self, unhashed_password: str) -> bool:
        return check_password_hash(self.password, unhashed_password)

    def __repr__(self):
        return f"<User {self.username}>"


class Project(db.Model):
    __tablename__ = 'projects'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), unique=True, index=True)
    short_tag = db.Column(db.String(10), unique=True, index=True)
    description = db.Column(db.Text, unique=True)
    owner_id = db.Column(db.Integer, db.ForeignKey('users.id'), index=True)
    rank = db.Column(db.Integer, index=True)

    created_at = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    last_updated_at = db.Column(db.DateTime, onupdate=datetime.datetime.utcnow)
    last_updated_by = db.Column(db.Integer, nullable=True)


class IssueType(db.Model):
    __tablename__ = 'issue_types'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(20), unique=True, index=True)
    rank = db.Column(db.Integer, index=True)
