from flask_alembic import Alembic
from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy
from flask_wtf.csrf import CSRFProtect


alembic = Alembic()
csrf = CSRFProtect()
db = SQLAlchemy()
login_manager = LoginManager()
