FROM python:3.8.1-slim-buster

LABEL MAINTAINER = "Amador Cuenca <sphi02ac@gmail.com>"

ENV INSTALL_PATH "/app"
RUN mkdir -p $INSTALL_PATH

WORKDIR $INSTALL_PATH

RUN apt-get update

COPY requirements.txt requirements.txt

RUN pip install --upgrade pip
RUN pip install -r requirements.txt

COPY . .

CMD gunicorn -b 0.0.0.0:8000 --access-logfile - "issue_tracker:create_app()"
